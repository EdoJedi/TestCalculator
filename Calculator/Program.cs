﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Calculator
{

  public class Tree
  {
    /// <summary>
    /// Tree class
    /// </summary>
    public Tree()
    {
      Value = null;
    }
    public Tree Parent { get; set; }
    public Tree Left { get; set; }
    public Tree Right { get; set; }
    public string Value { get; set; }
  }

  public class Program
  {
    /// <summary>
    /// Recursively build binary tree based on expression order
    /// </summary>
    /// <param name="idx"></param>
    /// <param name="nodes"></param>
    /// <param name="mathTree"></param>
    /// <returns></returns>
    public static Tree TreeBuild(int idx, List<string> nodes, Tree mathTree)
    {

      if (idx < nodes.Count)
      {
        mathTree.Value = nodes[idx];
        if (mathTree.Left == null)
        {
          mathTree.Left = new Tree();
        }
        if (string.IsNullOrEmpty(mathTree.Left.Value))
        {
          mathTree.Left.Parent = mathTree;
          mathTree.Left.Value = nodes[idx - 1];
        }
        //check if  the next 2 node is hitting the list limit
        if (idx + 2 < nodes.Count)
        {
          //check if the next 2 node is multiply or division
          //if it's multiply or division set the next node to right node
          if (nodes[idx + 2] == "*" || nodes[idx + 2] == "/")
          {
            Tree rTree = new Tree();
            rTree.Parent = mathTree;
            mathTree.Right = new Tree();
            mathTree.Right = TreeBuild(idx + 2, nodes, rTree);
          }

          //if it's not, set the right node as the next index value, 
          //then set the parent node of this node.
          //set the parent tree's left node to this node.
          else
          {
            mathTree.Right = new Tree();
            mathTree.Parent = new Tree();
            mathTree.Right.Value = nodes[idx + 1];

            Tree pTree = new Tree();
            pTree.Left = new Tree();
            pTree.Left = mathTree;
            pTree.Left.Value = nodes[idx];

            mathTree.Parent = TreeBuild(idx + 2, nodes, pTree);
          }
        }
        //if hit max node count, set the right node value instead
        else
        {
          mathTree.Right = new Tree();
          mathTree.Right.Value = nodes[idx + 1];
        }
      }
      return mathTree;
    }

    /// <summary>
    /// Get to the top of the tree.
    /// </summary>
    /// <param name="mathTree"></param>
    /// <returns></returns>
    public static Tree TopofTree(Tree mathTree)
    {
      if (mathTree.Parent != null)
      {
        return TopofTree(mathTree.Parent);
      }
      return mathTree;
    }

    /// <summary>
    /// Recursively calculate values in the tree.
    /// </summary>
    /// <param name="mathTree"></param>
    /// <returns></returns>
    public static double TreeCalculate(Tree mathTree)
    {
      //Check if current node is an expression, if not return value.
      if (mathTree.Value == "+" || mathTree.Value == "-" || mathTree.Value == "*" || mathTree.Value == "/")
      {
        double rval;
        //travel to right most node and get its value.
        if (!Double.TryParse(mathTree.Right.Value, out rval))
        {
          rval = TreeCalculate(mathTree.Right);
        }
        if (mathTree.Left != null)
        {

          //travel to left most node, calculate based on expression and return its value.
          if (mathTree.Value == "+")
          {
            return TreeCalculate(mathTree.Left) + rval;
          }
          if (mathTree.Value == "-")
          {
            return TreeCalculate(mathTree.Left) - rval;
          }
          if (mathTree.Value == "*")
          {
            return TreeCalculate(mathTree.Left) * rval;
          }
          if (mathTree.Value == "/")
          {
            return TreeCalculate(mathTree.Left) / rval;
          }
        }
        //return current node value if there's no more left node
        double val;
        Double.TryParse(mathTree.Value, out val);
        return val;
      }
      else
      {
        double val;
        Double.TryParse(mathTree.Value, out val);
        return val;
      }
    }

    /// <summary>
    /// Calculate equation based on given list of string.
    /// </summary>
    /// <param name="nodes"></param>
    /// <returns></returns>
    public static double TreeEquationCalc(List<string> nodes)
    {
      int eidx = 0;
      //get the index of the first expression as the first node to work with.
      for (int i = 0; i < nodes.Count(); i++)
      {
        if (nodes[i] == "+" || nodes[i] == "-" || nodes[i] == "*" || nodes[i] == "/")
        {
          eidx = i;
          break;
        }
      }
      Tree mathTree = new Tree();
      mathTree = TreeBuild(eidx, nodes, mathTree);
      Tree top = TopofTree(mathTree);
      return TreeCalculate(top);
    }

    /// <summary>
    /// Calculate equation with the addition of brackets.
    /// This function will get the last open bracket in the equation, 
    /// and the nearest closing bracket, get the sub list of string to perform binary tree calculation.
    /// </summary>
    /// <param name="openidx"></param>
    /// <param name="nodes"></param>
    /// <returns></returns>
    public static double BracketSlicing(Stack<int> openidx, List<string> nodes)
    {
      //recursive stop condition, if there's no more open brackets in the stack,
      //calculate the nodes using binary tree equation
      if (openidx.Count == 0)
      {
        return TreeEquationCalc(nodes);
      }
      int sbracket = openidx.Pop();
      int cbracket = 0;
      //Break when found the nearest close bracket.
      for (int i = sbracket; i < nodes.Count; i++)
      {
        if (nodes[i] == ")")
        {
          cbracket = i;
          break;
        }
      }
      //stop condition, if there's no close bracket
      //calculate the nodes using binary tree equation
      if (cbracket == 0)
      {
        return TreeEquationCalc(nodes);
      }
      //get the sub list of string inside the bracket to perform binary tree calculation
      string subNodeSum = TreeEquationCalc(nodes.GetRange(sbracket + 1, (cbracket - sbracket - 1))).ToString();

      //remove the content of the bracket and the bracket itselves.
      nodes.RemoveRange(sbracket, (cbracket - sbracket + 1));
      //insert the calculated answer for that bracket.
      nodes.Insert(sbracket, subNodeSum);
      //repeat til no more brackets.
      BracketSlicing(openidx, nodes);
      //calculate the whole equation after taking care of each bracket's value.
      return TreeEquationCalc(nodes);
    }

    /// <summary>
    /// Calculate equation.
    /// </summary>
    /// <param name="sum"></param>
    /// <returns></returns>
    public static double Calculate(string sum)
    {
      List<string> nodes = sum.Split(' ').ToList();
      //check if there's bracket, then put all the open brackets in a stack.
      if (sum.Contains('('))
      {
        Stack<int> openidx = new Stack<int>();

        for (int i = 0; i < nodes.Count(); i++)
        {
          if (nodes[i] == "(")
          {
            openidx.Push(i);
          }
        }
        return BracketSlicing(openidx, nodes);
      }
      //perform binary tree equation calculation when there's no bracket
      else
      {
        return TreeEquationCalc(nodes);
      }
    }

    static void Main(string[] args)
    {
      string test1 = "1 + 1";
      string test2 = "2 * 2";
      string test3 = "1 + 2 + 3";
      string test4 = "6 / 2";
      string test5 = "11 + 23";
      string test6 = "11.1 + 23";
      string test7 = "1 + 1 * 3";
      string test8 = "( 11.5 + 15.4 ) + 10.1";
      string test9 = "23 - ( 29.3 - 12.5 )";
      string test10 = "10 - ( 2 + 3 * ( 7 - 5 ) )";


      Console.WriteLine(test1 + " = " + Calculate(test1));
      Console.WriteLine(test2 + " = " + Calculate(test2));
      Console.WriteLine(test3 + " = " + Calculate(test3));
      Console.WriteLine(test4 + " = " + Calculate(test4));
      Console.WriteLine(test5 + " = " + Calculate(test5));
      Console.WriteLine(test6 + " = " + Calculate(test6));
      Console.WriteLine(test7 + " = " + Calculate(test7));
      Console.WriteLine(test8 + " = " + Calculate(test8));
      Console.WriteLine(test9 + " = " + Calculate(test9).ToString("0.#"));
      Console.WriteLine(test10 + " = " + Calculate(test10));


      Console.ReadLine();
    }
  }
}
